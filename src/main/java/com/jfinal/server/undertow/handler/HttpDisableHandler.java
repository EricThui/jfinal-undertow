/**
 * Copyright (c) 2011-2019, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jfinal.server.undertow.handler;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

/**
 * 用于在 https 开启时，关闭对 http 的访问
 * 
 * 配置方法：undertow.http.disable = true
 * 
 * 由于 https 在底层要走 http 通道，所以不能直接去掉 http 监听
 */
public class HttpDisableHandler  implements HttpHandler {
	
	private HttpHandler next;
	
	public HttpDisableHandler(HttpHandler next) {
		this.next = next;
	}
	
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		if ("http".equals(exchange.getRequestScheme())) {
			exchange.getResponseChannel().close();
		} else {
			next.handleRequest(exchange);
		}
	}
}





